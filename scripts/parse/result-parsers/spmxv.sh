#!/usr/bin/env zsh

# print header if no input file is given
if [ ${#} -eq 0 ]; then
    echo ',seconds,mflops'
    exit
fi

file=${1}
seconds=$(cat ${file} | grep 'Total experiment time' | awk '{ print $4 }')
mflops=$(cat ${file} | grep 'Total MFlops' | awk '{ print $3 }')
echo ",${seconds},${mflops}"
