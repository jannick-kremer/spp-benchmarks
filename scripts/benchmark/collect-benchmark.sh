#!/usr/bin/env zsh

human_date() { date +'%Y-%m-%d %H:%M:%S' }
file_date() { date +'%Y-%m-%dT%Hh%Mm%Ss' }

# argument parsing
# defaults for arguments
task=spmxv
version=release
num_executions=23
max_threads=256

usage() {
    echo "usage: collect-benchmark.sh [-t <task>] [-v <version>] [-n <num-executions>] [-h | --help]" 1>&2
    echo "" 1>&2
    echo "Collect raw benchmarking results" 1>&2
    echo "" 1>&2
    echo "script arguments:" 1>&2
    echo "    -h, --help" 1>&2
    echo "        show this help message and exit" 1>&2
    echo "    -t TASK, --task TASK" 1>&2
    echo "        the unique task identifier for which benchmarking is performed (default: ${task})" 1>&2
    echo "    -v VERSION, --version VERSION" 1>&2
    echo "        the unique identifier for the benchmarked code version (default: ${version})" 1>&2
    echo "    -n NUM_EXECUTIONS, --num-executions NUM_EXECUTIONS" 1>&2
    echo "        the number of executions for each thread count and problem size (default: ${num_executions})" 1>&2
    echo "    -m MAX_THREADS, --max-threads MAX_THREADS" 1>&2
    echo "        the highest number of threads to be tested, tests all powers of 2 between and including one and the given number (default: 256)"
}

while [ "$1" != "" ]; do
    case $1 in
        -t | --task )
            shift
            task=${1}
            ;;
        -v | --version )
            shift
            version=${1}
            ;;
        -d | --output-dir )
            shift
            output_dir=${1}
            ;;
        -n | --num-executions )
            shift
            num_executions=${1}
            ;;
        -m | --max-threads )
            shift
            max_threads=${1}
            ;;
        -h | --help )
            usage
            exit 0
            ;;
        * )
            usage
            exit 1
            ;;
    esac
    shift
done

readonly BASE_DIR=$(dirname $(dirname $(dirname $(readlink -f ${0}))))
readonly TASK_DIR=${BASE_DIR}/tasks/${task}
if [[ -z "$output_dir" ]]; then
    output_dir=${BASE_DIR}/benchmarking/${task}/$(file_date)-${version}
fi
readonly BASE_OUTPUT_DIR=$output_dir

echo "[$(human_date)] starting benchmark execution for task ${task} version ${version}..."

echo "[$(human_date)] writing output to ${BASE_OUTPUT_DIR}..."
mkdir -p ${BASE_OUTPUT_DIR}

if [ -f ${TASK_DIR}/setup-environment.sh ]; then
    echo "[$(human_date)] setting up the execution environment..."
    . ${TASK_DIR}/setup-environment.sh &> ${BASE_OUTPUT_DIR}/software.txt
fi

echo "[$(human_date)] collecting system information..."
echo -n "" > ${BASE_OUTPUT_DIR}/hardware.txt
echo "Hostname:              $(hostname)" &>> ${BASE_OUTPUT_DIR}/hardware.txt
lscpu &>> ${BASE_OUTPUT_DIR}/hardware.txt
numactl --hardware &>> ${BASE_OUTPUT_DIR}/hardware.txt
echo -n "" > ${BASE_OUTPUT_DIR}/operating-system.txt
lsb_release -d &>> ${BASE_OUTPUT_DIR}/operating-system.txt
echo "Kernel Release: $(uname -r)" &>> ${BASE_OUTPUT_DIR}/operating-system.txt

echo "[$(human_date)] rebuilding executable for task ${task}..."
cd ${TASK_DIR}
make clean build &> ${BASE_OUTPUT_DIR}/build.txt

for run in $(seq ${num_executions}); do
    for ((num_threads=1; num_threads<=$max_threads; num_threads=num_threads*2 )); do
        echo "[$(human_date)] running task ${task} execution ${run} with ${num_threads} threads..."
        output_dir=${BASE_OUTPUT_DIR}/${run}/${num_threads}
        mkdir -p ${output_dir}
        NTHREADS=${num_threads} make run-small &> ${output_dir}/small.txt
        NTHREADS=${num_threads} make run-large &> ${output_dir}/large.txt
    done
done
echo "[$(human_date)] finished benchmark execution..."
