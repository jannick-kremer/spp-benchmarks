# Benchmarking & Visualization

## Recommended Directory Hierarchy

```sh
BASE_DIR
├── renv
├── ...
├── scripts
│   ├── benchmark
│   │   └── collect-benchmark.sh
│   ├── parse
│   │   ├── parse-benchmark.sh
│   │   └── result-parsers
│   └── visualize
│       ├── visualize-benchmark.R
│       └── visualize-comparison.R
└── tasks
    ├── merge-sort
    ├── ...
    └── spmxv
```

## Instructions for Dependency Installation

```sh
$ R
> renv::init()
# press 1 to install the required libraries
```
