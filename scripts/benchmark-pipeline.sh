#!/usr/bin/env zsh

file_date() { date +'%Y-%m-%dT%Hh%Mm%Ss' }

# argument parsing
# defaults for arguments
task=kmeans
version=release
num_executions=23
max_threads=256

usage() {
    echo "usage: collect-benchmark.sh [-t <task>] [-v <version>] [-n <num-executions>] [-h | --help]" 1>&2
    echo "" 1>&2
    echo "Collect raw benchmarking results" 1>&2
    echo "" 1>&2
    echo "script arguments:" 1>&2
    echo "    -h, --help" 1>&2
    echo "        show this help message and exit" 1>&2
    echo "    -t TASK, --task TASK" 1>&2
    echo "        the unique task identifier for which benchmarking is performed (default: ${task})" 1>&2
    echo "    -v VERSION, --version VERSION" 1>&2
    echo "        the unique identifier for the benchmarked code version (default: ${version})" 1>&2
    echo "    -d DIRECTORY, --directory DIRECTORY" 1>&2
    echo "        the directory to save to and parse results from (default: ${directory})" 1>&2
    echo "    -n NUM_EXECUTIONS, --num-executions NUM_EXECUTIONS" 1>&2
    echo "        the number of executions for each thread count and problem size (default: ${num_executions})" 1>&2
    echo "    -m MAX_THREADS, --max-threads MAX_THREADS" 1>&2
    echo "        the highest number of threads to be tested, tests all powers of 2 between and including one and the given number (default: 256)"
}

while [ "$1" != "" ]; do
    case $1 in
        -t | --task )
            shift
            task=${1}
            ;;
        -v | --version )
            shift
            version=${1}
            ;;
        -d | --directory )
            shift
            directory=${1}
            ;;
        -n | --num-executions )
            shift
            num_executions=${1}
            ;;
        -m | --max-threads )
            shift
            max_threads=${1}
            ;;
        -h | --help )
            usage
            exit 0
            ;;
        * )
            usage
            exit 1
            ;;
    esac
    shift
done

readonly BASE_DIR=$(dirname $(dirname $(readlink -f ${0})))
if [[ -z "$directory" ]]; then
    directory=${BASE_DIR}/benchmarking/${task}/$(file_date)-${version}
else
    directory=${BASE_DIR}/benchmarking/${task}/${directory}
fi

if [ "$task" = "spmxv" ]; then
	column="mflops"
	metric="MFlops/s"
	invert="--invert-speedup"
elif [ "$task" = "merge-sort" -o "$task" = "kmeans" ]; then
	column="seconds"
	metric="Runtime"
	invert=""
fi

./scripts/benchmark/collect-benchmark.sh -t $task -v $version -d $directory -n $num_executions -m $max_threads
./scripts/parse/parse-benchmark.sh -t $task -v $version -d $directory -n $num_executions -m $max_threads
./scripts/visualize/visualize-benchmark.R --csv-file $directory/benchmark.csv --column-name seconds --metric-name Runtime
./scripts/visualize/visualize-comparison.R --csv-file $directory/benchmark.csv --column-name $column --metric-name Performance --metric-unit $metric $invert
