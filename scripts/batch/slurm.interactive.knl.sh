#!/usr/bin/env zsh

### Job name
#SBATCH --job-name=SWP_ManyCore_OpenMP
#SBATCH --account=<project-id>
###SBATCH --reservation=<advanced-reservation-id>

### File / path where STDOUT will be written, the %j is the job id
#SBATCH --output=output_%j.txt

### Optional: Send mail when job has finished
###SBATCH --mail-type=END
###SBATCH --mail-user=<email-address>

### Request time and memory
#SBATCH --time=02:00:00
#SBATCH --mem-per-cpu=1500

### Set Queue
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=64
#SBATCH --exclusive
#SBATCH --gres=knl

### wait for dispatch
sleep 7200
### execute 'ssh -Y <allocated-node>' from another shell window
### to get X11 forwarding and execute commands interactively
### when you are finished use 'scancel <job-id>' to free the resources
