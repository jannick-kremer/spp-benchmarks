#!/usr/bin/env zsh

### Job name
#SBATCH --job-name=SWP_ManyCore_OpenMP
#SBATCH --account=<project-id>
###SBATCH --reservation=<advanced-reservation-id>

### File / path where STDOUT will be written, the %j is the job id
#SBATCH --output=output_%j.txt

### Optional: Send mail when job has finished
###SBATCH --mail-type=END
###SBATCH --mail-user=<email-address>

### Request time and memory
#SBATCH --time=24:00:00
#SBATCH --mem-per-cpu=1500

### Set Queue
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=64
#SBATCH --exclusive
#SBATCH --gres=knl

BASE_DIR=~/Desktop/SWP_ManyCore_OpenMP
TASK=spmxv
VERSION=openmp

cd ${BASE_DIR}
./scripts/benchmark/collect-benchmark.sh --task ${TASK} --version ${VERSION}
